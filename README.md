# README #

This repository contains the RDF Turtle files for the IATI 2 LOD triple store, hosted at VU (http://semanticweb.cs.vu.nl/iati/). These are the result of a conversion effort by Kasper Brandt, for which the scripts can be found at https://github.com/KasperBrandt/IATI2LOD.

More information about the project, and live demonstrations can be seen at http://iati2lod.appspot.com/

contact Victor de Boer (v.de.boer@vu.nl) for more information
